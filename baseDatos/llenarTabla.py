import mysql.connector

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="",
  database="baseDePrueba"
)

mycursor = mydb.cursor()

sql = "INSERT INTO Canciones (song, artist, album) VALUES (%s, %s, %s)"
val = [('What you need', 'Yung Bae', 'Bae 5',), ('Welcome to the Disco', 'Yung Bae', 'Bae 5',), ('Fame - Fred Falke Rexmix', 'Axel Le Baron', 'Fame Remixes',),
('NUDIST', 'Kyoko Koizumi', 'Kyoko No Kiyoku Tanoshiku Utsukushiku',), ('Love Taste', 'Moe Shop', 'Love Taste',), ('Feel Me', 'Sui Uzi', 'Feel Me',),
('Midnight Sailor', 'Sui Uzi', 'Night Songs Pt. 1',)]

mycursor.executemany(sql, val)

mydb.commit()