import mysql.connector
import pandas as pd
import sqlite3

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="",
  database = "alumnos"
)

mycursor = mydb.cursor()    

#mycursor.execute("CREATE TABLE informacion (id INT, name VARCHAR(255), expediente INT, edad INT, carrera VARCHAR(255))")

'''
sql = "INSERT INTO informacion (id, name, expediente, edad, carrera ) VALUES (%s, %s, %s, %s, %s)"
val = [('1', 'Hernandez Garcia Carlos Zahid', '307077', '19', 'Ingenieria de Software'), 
('2', 'Anaya Hurtado Rodrigo', '307097', '19', 'Ingenieria de Software'), 
('3', 'Diaz Villegas Erick', '307011', '19', 'Ingenieria de Software'), 
('4', 'Arredondo Moreno Mauricio', '307066', '19', 'Ingenieria de Software'),
('5', 'Benitez Morales Pedro Oswaldo', '307072', '19', 'Ingenieria de Software'), 
('6', 'Gomez Orizaga Sebastian Alberto', '307083', '19', 'Ingenieria de Software'), 
('7', 'Melgar Zarrabal Cristian Julian', '273304', '19', 'Ingenieria de Software'), 
('8', 'Olvera Perez Francisco', '307040', '19', 'Ingenieria de Software'), 
('9', 'Ordaz Rebollo Ximena', '307046', '19', 'Ingenieria de Software'), 
('10','Ortiz Villafuerte Alesandra Beyonce', '307101', '19', 'Ingenieria de Software'), 
('11','Ramirez Hernandez Hugo Alejandro', '307058', '19', 'Ingenieria de Software'), 
('12', 'Rodriguez Nieto Carlos Tomas', '307089', '19', 'Ingenieria de Software'),
('13', 'Romero Resendiz Miguel Angel', '278883', '19', 'Ingenieria de Software')]
mycursor.executemany(sql, val)
mydb.commit()
'''


df1 = pd.read_sql_query("SELECT * FROM informacion", mydb)

df2 = pd.read_json('results.json')
df2 = df2.drop('Name', axis= 1)

df3 = pd.merge(df1, df2, how='left', on='id')
df3.to_excel("segs.xlsx")
df3.head()
